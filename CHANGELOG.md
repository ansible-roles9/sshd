# [1.3.0](/Ansible_Roles/sshd/compare/1.3.0...master)
* CICD migraton from Jenkins to GitlabCI
* Update exceptions for *handlers* in order to work with *Gitlab*

# [1.2.0](/Ansible_Roles/sshd/compare/1.2.0...master)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se actualiza el *Jenkinsfile* para con las variables que son necesarias por la versión actual del **Shared libraries**
* Se actualiza *meta* indicando la versión de `ansible` máxima soportada.

# [1.1.0](/Ansible_Roles/sshd/compare/1.1.0...master)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [1.0.2](/Ansible_Roles/sshd/compare/1.0.2...master)
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [1.0.1](/Ansible_Roles/sshd/compare/1.0.1...master)
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [1.0.0](/Ansible_Roles/sshd/compare/1.0.0...master)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante

# [0.3.0](/Ansible_Roles/sshd/compare/0.3.0...master)
* Se retira la opción `UsePrivilegeSeparation` Y `RhostsRSAAuthentication` al quedar deprecadas en versiones modernas de **openssh-server**.
* Comentamos temporalmente el hook para crear `merge_requests` ya que se ha de actualizar para que los haga en `gitea` y no en `gitlab`

# [0.2.2](/Ansible_Roles/sshd/compare/0.2.2...master)
* Se corrige el error **'Match Address' in configuration but 'addr'** cuando se validaba la sintaxis en los bloques de configuración.
* Se corrige tambien el problema de iteración de la variable **blocks**, ahora se trata como una sola `string multilínea`

# [0.2.1](/Ansible_Roles/sshd/compare/0.2.1...master)
* Se hacen mas amigables las búsquedas de *keys* dejando de usar `regexp en las iteraciones`, estan se pasan al *background*
* Se elimina código residual del rol.
* Se ajusta el hardening por defecto del servicio sshd

# [0.2.0](/Ansible_Roles/sshd/compare/0.2.0...master)
* Se crean 2 nuevas variables, options y blocks:
    - `options`, sustituye a *updt_conf_lines*; busca la clave en el fichero de configuración y asigna el valor que se indica
    - `blocks`, busca un bloque en el fichero de configuración y asigna el valor que se indica, útil para configuraciones tipo **Match**
* Se reemplaza el uso de **fichero** por una **variable** para el contenido del `sshd_banner`

# [0.1.1](/Ansible_Roles/sshd/compare/0.1.1...master)
* Actualizamos `Jenkinsfile` para adaptarnos a la nueva versión del vars `ansibleActions`[jenkins-shared-libraries]

# [0.1.0](/Ansible_Roles/sshd/compare/0.1.0...master)
* Se inicializa el *CHANGELOG* para un mejor seguimiento de los cambios
* Se activa el **update_cache** para cada vez que se haga la instalación de requerimientos
* Se actualiza el *Jenkinsfile* con la configuración adaptada para las nuevas librerías
* Se sube el valor de la variable *MaxAuthTries* de 2 a 5
* Se corrige *bug* en el handler que provocaba que en ves de asegurarse que el servicio esté *reiniciado* cuando hay un cambio, se aseguraba que solo este *iniciado*
