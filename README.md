[![pipeline status](https://gitlab.com/ansible-roles9/sshd/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/sshd/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/sshd)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/sshd)

Role SSHD
===================

Description
-------------
The purpose of this role is to configure the `SSHD service`, both at the *hardening* and *configuration* levels. For each new insertion it makes, it validates the syntax of the configuration file.

Requirements
-------------
It has no special requirements

How to use
-------------
    - import_role:
        name: "sshd"
      vars:
        options:
          - {key: "Clave sshd", value: "Valor"}
        blocks: |
          Match User ansible-agent
            PasswordAuthentication no

ROOT Vars
-------------

* Variable name: `options`
* Default value: empty [].
* Accepted values: Dictonary key => value [ soporta Regex ].
* Description: Variable intended to contain the list of rules.

----------

* Variable name: `blocks`
* Default value: empty.
* Accepted values: STRING BLOCK
* Description: Block configuration in **sshd** configuration.

CHILD Vars for: `options`
-------------

* Variable name: `key`
* Default value: empty.
* Accepted values: Dictionaries list [key => value].
* Description: Key to match in **sshd** configuration.

----------

* Variable name: `value`
* Default value: empty.
* Accepted values: Dictionaries list [key => value].
* Description: Value desired.


Legend
-------------
* NKS => No key sensitive
* KS => Key sensitive
